import pickle
from typing import List
import mlflow
import click
from dotenv import dotenv_values

env_values = dotenv_values(".env")
mlflow.set_tracking_uri(
    f"http://{env_values['MLFLOW_NGINX_NAME']}:"
    f"{env_values['MLFLOW_NGINX_PWD']}@16.16.194.244:5000/")
mlflow.set_experiment('dummy_model')


class DummySimilarityModel(mlflow.pyfunc.PythonModel):
    def predict(self, context, model_input: List[str]) -> List[str]:
        return self.predict_function()

    def predict_function(self) -> List[str]:
        # do something with the model input
        return ["I'm a model output!"]


@click.command()
@click.option("--train_data_path", type=click.Path(exists=True))
@click.option("--result_model_path", type=click.Path())
def main(train_data_path, result_model_path):
    with open(train_data_path, "r") as f:
        pass
    model = DummySimilarityModel()
    with mlflow.start_run():
        model_info = \
            mlflow.pyfunc.log_model(artifact_path="model", python_model=model)

    loaded_model = mlflow.pyfunc.load_model(model_uri=model_info.model_uri)
    print(model_info.model_uri)
    print(type(loaded_model))  # <class 'mlflow.pyfunc.model.PyFuncModel'>

    unwrapped_model = loaded_model.unwrap_python_model()
    print(type(unwrapped_model))  # <class '__main__.MyModel'>
    print(unwrapped_model.predict_function([['a', 'b']]))  # works

    print(loaded_model.predict([['a', 'b'], ['c', 'd']]))
    with open(result_model_path, "wb") as f:
        pickle.dump("SOTA model is brewing", f)


if __name__ == '__main__':
    main()
