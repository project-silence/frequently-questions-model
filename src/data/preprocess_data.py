import click


@click.command()
@click.option("--raw_data_path", type=click.Path(exists=True))
@click.option("--prepared_data_path", type=click.Path())
def main(raw_data_path, prepared_data_path):
    with open(raw_data_path, "r") as f:
        print(f.readline())
    with open(prepared_data_path, "w") as f:
        f.write("Preprocessed data to be saved here")


if __name__ == '__main__':
    main()
