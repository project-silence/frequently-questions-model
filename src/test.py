import hydra
from omegaconf import DictConfig, OmegaConf


def print_conf(config: DictConfig):
    print(OmegaConf.to_yaml(config))


@hydra.main(version_base=None, config_path="../config", config_name="config")
def my_app(cfg: DictConfig) -> None:
    print_conf(cfg)


if __name__ == "__main__":
    my_app(DictConfig({}))
